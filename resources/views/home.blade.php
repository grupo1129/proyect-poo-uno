@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">OPCIONES DEL SISTEMA</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div class="col text-left">
                        <a href="{{route('lista.productos')}}" class="btn btn-sm btn-primary">Producto</a>                    
                        <a href="{{ route('lista.cliente')}}" class="btn btn-sm btn-primary">Clientes</a>                    
                        <a href="" class="btn btn-sm btn-primary">Facturas</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection